# CHANGELOG

## 1.0.5 ()

* [`harbor`](./roles/harbor/) new variable `harbor_install_rerun`.
* [`backup`](./roles/backup/) replaced `ansible_role_names_cleaned` with
  `ansible_play_role_names` and `backup_roles_supported` using FQCN now.
* [`backup`](./roles/backup/) added support for backup of containers volumes.
* [`backup`](./roles/backup/) fix `lookup` of `vars`.
* [`splunk`](./roles/splunk/) started new role for Splunk.
* `c2platform.core` → `1.0.23`

## 1.0.4 ( 2024-06-26 )

* `c2platform.core` → `1.0.22`
* Added filter / module documentation.
* `.gitlab-ci.yml` based on `c2platform.core`.
* [`awx`](./roles/awx/) fixed `tags`.
* Replaced `awx_config_file_path`, `awx_config_file` with `awx_connection` and
  `awx_connection_file`.
* Update dependencies: `c2platform.core: ">=1.0.20"` and `awx.awx: ">=24.3.1"`.
* [`awx`](./roles/awx/) added all modules from `awx.awx` collection.
* [`awx`](./roles/awx/) add attributes `scm_branch`, `ask_scm_branch_on_launch`,
  `allow_override` and `execution_environment`.
* [`awx`](./roles/awx/) using `c2platform.core.resources` filter.
* [`awx`](./roles/awx/) retries 30.

## 1.0.3 ( 2024-03-08 )

* [`backup`](./roles/backup/) improved restore logging

## 1.0.2 ( 2023-09-29 )

* [`awx`](./roles/awx) added dict `awx_alive_check` so we can configure to wait
  until API becomes  available.
* [`awx`](./roles/awx) added `job_tags`.

## 1.0.1 ( 2023-08-09 )

* [`awx`](./roles/awx) completely new `awx` role to manage [Ansible Automation
  Platform (AAP) or AWX](https://c2platform.org/docs/concepts/ansible/aap/).

## 1.0.0 ( 2023-06-16 )

* New pipeline script.

## 0.1.4 ( 2022-10-25 )

* [`server_update`](./roles/server_update) removed, moved to
  [`c2platform.core.server_update`](https://gitlab.com/c2platform/ansible-collection-core/-/tree/master/roles/server_update/README.md).

## 0.1.3 ( 2022-10-02 )

* [`server_update`](./roles/server_update) fix

## 0.1.2 ( 2022-09-19 )

* New [`harbor`](./roles/harbor) role.

## 0.1.1 ( 2022-09-14 )

* Improved `.gitlab-ci.yml`

## 0.1.0 ( 2022-09-14 )

* Initial release

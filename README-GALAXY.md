# Ansible Collection - c2platform.mgmt

[![Pipeline Status](https://gitlab.com/c2platform/ansible-collection-mgmt/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/ansible-collection-mgmt/-/pipelines) [![Latest Release](https://gitlab.com/c2platform/ansible-collection-mgmt/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/ansible-collection-mgmt/-/pipelines)

See [README](https://gitlab.com/c2platform/ansible-collection-mgmt/-/blob/master/README.md).

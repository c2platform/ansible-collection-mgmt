# Ansible Role c2platform.mgmt.awx

This role can be used to manage [Ansible Automation Platform (AAP) or AWX](https://c2platform.org/en/docs/concepts/ansible/aap/) using Ansible collection [awx.awx](https://docs.ansible.com/ansible/latest/collections/awx/awx/index.html).

- [Requirements](#requirements)
- [Role Variables](#role-variables)
  - [`awx_connection_file`: Create controller config file](#awx_connection_file-create-controller-config-file)
  - [`awx_connection`: Configure connection](#awx_connection-configure-connection)
  - [`awx_alive_check`: AWX availability check](#awx_alive_check-awx-availability-check)
  - [Modules](#modules)
    - [`ad_hoc_command`: create, update, or destroy Automation Platform Controller ad hoc commands.](#ad_hoc_command-create-update-or-destroy-automation-platform-controller-ad-hoc-commands)
    - [`ad_hoc_command_cancel`: Cancel an Ad Hoc Command.](#ad_hoc_command_cancel-cancel-an-ad-hoc-command)
    - [`ad_hoc_command_wait`: Wait for Automation Platform Controller Ad Hoc Command to finish.](#ad_hoc_command_wait-wait-for-automation-platform-controller-ad-hoc-command-to-finish)
    - [`application`: create, update, or destroy Automation Platform Controller applications](#application-create-update-or-destroy-automation-platform-controller-applications)
    - [`bulk_host_create`: Bulk host create in Automation Platform Controller](#bulk_host_create-bulk-host-create-in-automation-platform-controller)
    - [`bulk_job_launch`: Bulk job launch in Automation Platform Controller](#bulk_job_launch-bulk-job-launch-in-automation-platform-controller)
    - [`controller_meta`: Returns metadata about the collection this module lives in.](#controller_meta-returns-metadata-about-the-collection-this-module-lives-in)
    - [`credential`: create, update, or destroy Automation Platform Controller credential.](#credential-create-update-or-destroy-automation-platform-controller-credential)
    - [`credential_input_source`: create, update, or destroy Automation Platform Controller credential input sources.](#credential_input_source-create-update-or-destroy-automation-platform-controller-credential-input-sources)
    - [`credential_type`: Create, update, or destroy custom Automation Platform Controller credential type.](#credential_type-create-update-or-destroy-custom-automation-platform-controller-credential-type)
    - [`execution_environment`: create, update, or destroy Execution Environments in Automation Platform Controller.](#execution_environment-create-update-or-destroy-execution-environments-in-automation-platform-controller)
    - [`export`: export resources from Automation Platform Controller.](#export-export-resources-from-automation-platform-controller)
    - [`group`: create, update, or destroy Automation Platform Controller group.](#group-create-update-or-destroy-automation-platform-controller-group)
    - [`host`: create, update, or destroy Automation Platform Controller host.](#host-create-update-or-destroy-automation-platform-controller-host)
    - [`import`: import resources into Automation Platform Controller.](#import-import-resources-into-automation-platform-controller)
    - [`instance`: create, update, or destroy Automation Platform Controller instances.](#instance-create-update-or-destroy-automation-platform-controller-instances)
    - [`instance_group`: create, update, or destroy Automation Platform Controller instance groups.](#instance_group-create-update-or-destroy-automation-platform-controller-instance-groups)
    - [`inventory`: create, update, or destroy Automation Platform Controller inventory.](#inventory-create-update-or-destroy-automation-platform-controller-inventory)
    - [`inventory_source`: create, update, or destroy Automation Platform Controller inventory source.](#inventory_source-create-update-or-destroy-automation-platform-controller-inventory-source)
    - [`inventory_source_update`: Update inventory source(s).](#inventory_source_update-update-inventory-sources)
    - [`job_cancel`: Cancel an Automation Platform Controller Job.](#job_cancel-cancel-an-automation-platform-controller-job)
    - [`job_launch`: Launch an Ansible Job.](#job_launch-launch-an-ansible-job)
    - [`job_list`: List Automation Platform Controller jobs.](#job_list-list-automation-platform-controller-jobs)
    - [`job_template`: create, update, or destroy Automation Platform Controller job templates.](#job_template-create-update-or-destroy-automation-platform-controller-job-templates)
    - [`job_wait`: Wait for Automation Platform Controller job to finish.](#job_wait-wait-for-automation-platform-controller-job-to-finish)
    - [`label`: create, update, or destroy Automation Platform Controller labels.](#label-create-update-or-destroy-automation-platform-controller-labels)
    - [`license`: Set the license for Automation Platform Controller](#license-set-the-license-for-automation-platform-controller)
    - [`notification_template`: create, update, or destroy Automation Platform Controller notification.](#notification_template-create-update-or-destroy-automation-platform-controller-notification)
    - [`organization`: create, update, or destroy Automation Platform Controller organizations](#organization-create-update-or-destroy-automation-platform-controller-organizations)
    - [`project`: create, update, or destroy Automation Platform Controller projects](#project-create-update-or-destroy-automation-platform-controller-projects)
    - [`project_update`: Update a Project in Automation Platform Controller](#project_update-update-a-project-in-automation-platform-controller)
    - [`role`: grant or revoke an Automation Platform Controller role.](#role-grant-or-revoke-an-automation-platform-controller-role)
    - [`schedule`: create, update, or destroy Automation Platform Controller schedules.](#schedule-create-update-or-destroy-automation-platform-controller-schedules)
    - [`settings`: Modify Automation Platform Controller settings.](#settings-modify-automation-platform-controller-settings)
    - [`subscriptions`: Get subscription list](#subscriptions-get-subscription-list)
    - [`team`: create, update, or destroy Automation Platform Controller team.](#team-create-update-or-destroy-automation-platform-controller-team)
    - [`token`: create, update, or destroy Automation Platform Controller tokens.](#token-create-update-or-destroy-automation-platform-controller-tokens)
    - [`user`: create, update, or destroy Automation Platform Controller users.](#user-create-update-or-destroy-automation-platform-controller-users)
    - [`workflow_approval`: Approve an approval node in a workflow job.](#workflow_approval-approve-an-approval-node-in-a-workflow-job)
    - [`workflow_job_template`: create, update, or destroy Automation Platform Controller workflow job templates.](#workflow_job_template-create-update-or-destroy-automation-platform-controller-workflow-job-templates)
    - [`workflow_job_template_node`: create, update, or destroy Automation Platform Controller workflow job template nodes.](#workflow_job_template_node-create-update-or-destroy-automation-platform-controller-workflow-job-template-nodes)
    - [`workflow_launch`: Run a workflow in Automation Platform Controller](#workflow_launch-run-a-workflow-in-automation-platform-controller)
    - [`workflow_node_wait`: Wait for a workflow node to finish.](#workflow_node_wait-wait-for-a-workflow-node-to-finish)
- [Dependencies](#dependencies)
- [Example Playbook](#example-playbook)

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

### `awx_connection_file`: Create controller config file

```yaml
awx_connection_file:
  path: /root/tower_cli.cfg
  owner: root
  group: root
  content: |
    host=awx.{{ gs_domain_name }}
    port=443
    verify_ssl=false
    username={{ awx_username }}
    password={{ awx_password }}
```

### `awx_connection`: Configure connection

If you want to use a controller config file:

```yaml
awx_connection:
  controller_config_file: /root/tower_cli.cfg
```

If you don't want to use a controller config file:

```yaml
awx_connection:
  controller_host: "awx.{{ gs_domain_name }}"
  controller_password: "{{ awx_username }}"
  controller_username: "{{ awx_password }}"
```

```yaml
awx_connection:
  controller_oauthtoken: <token>
```

### `awx_alive_check`: AWX availability check

Configure the `awx_alive_check` dictionary to ensure the availability of the AWX
REST API before proceeding with the AWX configuration.

```yaml
awx_alive_check:
  url: "https://awx.c2platform.org:443/api"
  url_username: "{{ awx_username }}"
  url_password: "{{ awx_password }}"
  status_code: 200
  return_content: true
  # timeout: 300
  validate_certs: false
  retries: 200
  delay: 30
  until_content: AWX REST API
  enabled: true
```

### Modules

<!-- start supported_modules -->
This roles supports a selection of 43 modules
from the collections `awx.awx`. This section
contains simple examples on how these modules can be used via the `awx_resources` and
`awx_<module_name>` variables.

#### `ad_hoc_command`: create, update, or destroy Automation Platform Controller ad hoc commands.

```yaml
awx_ad_hoc_command:
  - inventory: "Demo Inventory"
    credential: "Demo Credential"
    module_name: "command"
    module_args: "echo I <3 Ansible"
    wait: true
  - inventory: "Another Inventory"
    credential: "Another Credential"
    module_name: "shell"
    module_args: "uptime"
    become_enabled: true
    wait: false

```

```yaml
awx_resources:
  - module: ad_hoc_command
    inventory: "Demo Inventory"
    credential: "Demo Credential"
    module_name: "command"
    module_args: "echo I <3 Ansible"
    wait: true
  - module: ad_hoc_command
    inventory: "Another Inventory"
    credential: "Another Credential"
    module_name: "shell"
    module_args: "uptime"
    become_enabled: true
    wait: false

```

For more examples and information on options see
[`awx.awx.ad_hoc_command`](https://docs.ansible.com/ansible/latest/collections/awx/awx/ad_hoc_command_module.html).
#### `ad_hoc_command_cancel`: Cancel an Ad Hoc Command.

```yaml
awx_ad_hoc_command_cancel:
  - command_id: 123
    controller_host: "https://example.com"
    controller_username: "admin"
    controller_password: "password"
    validate_certs: true
  - command_id: 456
    controller_host: "https://example.com"
    controller_oauthtoken: "bqV5txm97wqJqtkxlMkhQz0pKhRMMX"
    interval: 2.0
    timeout: 60

```

```yaml
awx_resources:
  - module: ad_hoc_command_cancel
    command_id: 789
    controller_host: "https://example.com"
    controller_username: "admin"
    controller_password: "password"
    fail_if_not_running: true
  - module: ad_hoc_command_cancel
    command_id: 101
    controller_host: "https://example.com"
    controller_oauthtoken: "bqV5txm97wqJqtkxlMkhQz0pKhRMMX"
    timeout: 120
    validate_certs: false

```

For more examples and information on options see
[`awx.awx.ad_hoc_command_cancel`](https://docs.ansible.com/ansible/latest/collections/awx/awx/ad_hoc_command_cancel_module.html).
#### `ad_hoc_command_wait`: Wait for Automation Platform Controller Ad Hoc Command to finish.

```yaml
# Example using awx_ad_hoc_command_wait
awx_ad_hoc_command_wait:
  - command_id: 123
    controller_host: "https://controller.example.com"
    controller_username: "admin"
    controller_password: "password"
    timeout: 120
  - command_id: 456
    controller_host: "https://controller.example2.com"
    controller_oauthtoken: "bqV5txm97wqJqtkxlMkhQz0pKhRMMX"
    interval: 5

```

```yaml
# Example using awx_resources
awx_resources:
  - module: ad_hoc_command_wait
    command_id: 789
    controller_config_file: "/path/to/config/file"
    request_timeout: 30
  - module: ad_hoc_command_wait
    command_id: 321
    controller_host: "https://controller.example3.com"
    controller_username: "user"
    controller_password: "securepassword"
    timeout: 180

```

For more examples and information on options see
[`awx.awx.ad_hoc_command_wait`](https://docs.ansible.com/ansible/latest/collections/awx/awx/ad_hoc_command_wait_module.html).
#### `application`: create, update, or destroy Automation Platform Controller applications

```yaml
awx_application:
  - name: "Foo"
    description: "Foo bar application"
    organization: "test"
    state: present
    authorization_grant_type: password
    client_type: public
    controller_host: "http://localhost"
    controller_username: "admin"
    controller_password: "password"
  - name: "Bar"
    description: "Bar application"
    organization: "test"
    state: present
    authorization_grant_type: authorization-code
    client_type: confidential
    redirect_uris:
      - "http://tower.com/api/v2/"
    controller_host: "http://localhost"
    controller_username: "admin"
    controller_password: "password"

```

```yaml
awx_resources:
  - module: application
    name: "Foo"
    description: "Foo bar application"
    organization: "test"
    state: present
    authorization_grant_type: password
    client_type: public
    controller_host: "http://localhost"
    controller_username: "admin"
    controller_password: "password"
  - module: application
    name: "Bar"
    description: "Bar application"
    organization: "test"
    state: present
    authorization_grant_type: authorization-code
    client_type: confidential
    redirect_uris:
      - "http://tower.com/api/v2/"
    controller_host: "http://localhost"
    controller_username: "admin"
    controller_password: "password"

```

For more examples and information on options see
[`awx.awx.application`](https://docs.ansible.com/ansible/latest/collections/awx/awx/application_module.html).
#### `bulk_host_create`: Bulk host create in Automation Platform Controller

```yaml
# Using the awx_bulk_host_create variable
awx_bulk_host_create:
  - inventory: 1
    hosts:
      - name: server1.domain.com
        description: "Web server"
        enabled: true
      - name: server2.domain.com
        description: "Database server"
        enabled: true
    controller_host: "https://awx.example.com"
    controller_username: "admin"
    controller_password: "password"
    validate_certs: false

```

```yaml
# Using the awx_resources variable
awx_resources:
  - module: bulk_host_create
    inventory: 1
    hosts:
      - name: server1.domain.com
        description: "Web server"
        enabled: true
      - name: server2.domain.com
        description: "Database server"
        enabled: true
    controller_host: "https://awx.example.com"
    controller_username: "admin"
    controller_password: "password"
    validate_certs: false

```

For more examples and information on options see
[`awx.awx.bulk_host_create`](https://docs.ansible.com/ansible/latest/collections/awx/awx/bulk_host_create_module.html).
#### `bulk_job_launch`: Bulk job launch in Automation Platform Controller

```yaml
awx_bulk_job_launch:
  - name: My Bulk Job Launch
    jobs:
      - unified_job_template: 7
        skip_tags: foo
      - unified_job_template: 10
        limit: foo
        extra_data:
          food: carrot
          color: orange
    limit: bar
    credentials:
      - "My Credential"
      - "supplementary cred"
    extra_vars: # these override / extend extra_data at the job level
      food: grape
      animal: owl
    organization: Default
    inventory: Demo Inventory
  - name: Another Bulk Job Launch
    jobs:
      - unified_job_template: 7
  wait: true

```

```yaml
awx_resources:
  - module: bulk_job_launch
    name: My Bulk Job Launch
    jobs:
      - unified_job_template: 7
        skip_tags: foo
      - unified_job_template: 10
        limit: foo
        extra_data:
          food: carrot
          color: orange
    limit: bar
    credentials:
      - "My Credential"
      - "supplementary cred"
    extra_vars: # these override / extend extra_data at the job level
      food: grape
      animal: owl
    organization: Default
    inventory: Demo Inventory
  - module: bulk_job_launch
    name: Another Bulk Job Launch
    jobs:
      - unified_job_template: 7
    wait: true

```

For more examples and information on options see
[`awx.awx.bulk_job_launch`](https://docs.ansible.com/ansible/latest/collections/awx/awx/bulk_job_launch_module.html).
#### `controller_meta`: Returns metadata about the collection this module lives in.

```yaml
awx_controller_meta:
  - controller_host: "https://controller.example.com"
    controller_username: "admin"
    controller_password: "password"
    request_timeout: 15
    validate_certs: false
  - controller_config_file: "/path/to/config/file"
    controller_oauthtoken: "your-oauth-token"
    request_timeout: 20
    validate_certs: true

```

```yaml
awx_resources:
  - module: controller_meta
    controller_host: "https://controller.example.com"
    controller_username: "admin"
    controller_password: "password"
    request_timeout: 15
    validate_certs: false
  - module: controller_meta
    controller_config_file: "/path/to/config/file"
    controller_oauthtoken: "your-oauth-token"
    request_timeout: 20
    validate_certs: true

```

For more examples and information on options see
[`awx.awx.controller_meta`](https://docs.ansible.com/ansible/latest/collections/awx/awx/controller_meta_module.html).
#### `credential`: create, update, or destroy Automation Platform Controller credential.

```yaml
# Using the awx_credential variable:
awx_credential:
  - name: Machine Credential
    description: "Description for Machine Credential"
    credential_type: "Machine"
    organization: "Default"
    inputs:
      username: "my_username"
      password: "my_password"
    state: "present"
    controller_config_file: "/path/to/controller/config"

  - name: SCM Credential
    organization: "Default"
    credential_type: "Source Control"
    inputs:
      username: joe
      password: secret
      ssh_key_data: "{{ lookup('file', '/tmp/id_rsa') }}"
      ssh_key_unlock: "passphrase"
    state: present

```

```yaml
# Using the awx_resources variable:
awx_resources:
  - module: credential
    name: Machine Credential
    description: "Description for Machine Credential"
    credential_type: "Machine"
    organization: "Default"
    inputs:
      username: "my_username"
      password: "my_password"
    state: "present"
    controller_config_file: "/path/to/controller/config"

  - module: credential
    name: SCM Credential
    organization: "Default"
    credential_type: "Source Control"
    inputs:
      username: joe
      password: secret
      ssh_key_data: "{{ lookup('file', '/tmp/id_rsa') }}"
      ssh_key_unlock: "passphrase"
    state: present

```

For more examples and information on options see
[`awx.awx.credential`](https://docs.ansible.com/ansible/latest/collections/awx/awx/credential_module.html).
#### `credential_input_source`: create, update, or destroy Automation Platform Controller credential input sources.

```yaml
awx_credential_input_source:
  - input_field_name: password
    target_credential: new_cred
    source_credential: cyberark_lookup
    metadata:
      object_query: "Safe=MY_SAFE;Object=awxuser"
      object_query_format: "Exact"
    state: present

  - input_field_name: aws_access_key
    target_credential: aws_key
    source_credential: aws_vault
    metadata:
      vault_name: "MyVault"
    state: present

```

```yaml
awx_resources:
  - module: credential_input_source
    input_field_name: password
    target_credential: new_cred
    source_credential: cyberark_lookup
    metadata:
      object_query: "Safe=MY_SAFE;Object=awxuser"
      object_query_format: "Exact"
    state: present

  - module: credential_input_source
    input_field_name: aws_access_key
    target_credential: aws_key
    source_credential: aws_vault
    metadata:
      vault_name: "MyVault"
    state: present

```

For more examples and information on options see
[`awx.awx.credential_input_source`](https://docs.ansible.com/ansible/latest/collections/awx/awx/credential_input_source_module.html).
#### `credential_type`: Create, update, or destroy custom Automation Platform Controller credential type.

```yaml
awx_credential_type:
  - name: Nexus
    description: Credentials type for Nexus
    kind: cloud
    inputs: "{{ lookup('file', 'credential_inputs_nexus.json') }}"
    injectors: {'extra_vars': {'nexus_credential': 'test' }}
    state: present
    validate_certs: false
  - name: Nexus
    state: absent

```

```yaml
awx_resources:
  - module: credential_type
    name: Nexus
    description: Credentials type for Nexus
    kind: cloud
    inputs: "{{ lookup('file', 'credential_inputs_nexus.json') }}"
    injectors: {'extra_vars': {'nexus_credential': 'test' }}
    state: present
    validate_certs: false
  - module: credential_type
    name: Nexus
    state: absent

```

For more examples and information on options see
[`awx.awx.credential_type`](https://docs.ansible.com/ansible/latest/collections/awx/awx/credential_type_module.html).
#### `execution_environment`: create, update, or destroy Execution Environments in Automation Platform Controller.

```yaml
awx_execution_environment:
  - name: "My EE"
    image: "quay.io/ansible/awx-ee"
    organization: "Default"
    state: "present"
  - name: "Another EE"
    image: "quay.io/ansible/my-awx-ee"
    organization: "Development"
    state: "present"

```

```yaml
awx_resources:
  - module: execution_environment
    name: "My EE"
    image: "quay.io/ansible/awx-ee"
    organization: "Default"
    state: "present"
  - module: some_other_module
    name: "Other Resource"
    description: "Description for other module"

```

For more examples and information on options see
[`awx.awx.execution_environment`](https://docs.ansible.com/ansible/latest/collections/awx/awx/execution_environment_module.html).
#### `export`: export resources from Automation Platform Controller.

```yaml
awx_export:
  - all: True
  - inventory:
      - "My Inventory 1"
      - "My Inventory 2"

```

```yaml
awx_resources:
  - module: export
    all: True
  - module: export
    job_templates:
      - "My Template"
    credentials: 'all'

```

```yaml
win_package:
  - name: "Notepad++"
    path: "C:\\path\\to\\notepadpp_installer.exe"
    state: "present"

```

```yaml
win_resources:
  - module: win_package
    name: "Notepad++"
    path: "C:\\path\\to\\notepadpp_installer.exe"
    state: "present"

```

For more examples and information on options see
[`awx.awx.export`](https://docs.ansible.com/ansible/latest/collections/awx/awx/export_module.html).
#### `group`: create, update, or destroy Automation Platform Controller group.

```yaml
awx_group:
  - name: localhost
    description: "Local Host Group"
    inventory: "Local Inventory"
    state: present
    controller_config_file: "~/tower_cli.cfg"
  - name: Cities
    description: "Host Group for Cities"
    inventory: Default Inventory
    hosts:
      - fda
    children:
      - NewYork
    preserve_existing_hosts: True
    preserve_existing_children: True

```

```yaml
awx_resources:
  - module: group
    name: localhost
    description: "Local Host Group"
    inventory: "Local Inventory"
    state: present
    controller_config_file: "~/tower_cli.cfg"
  - module: group
    name: Cities
    description: "Host Group for Cities"
    inventory: Default Inventory
    hosts:
      - fda
    children:
      - NewYork
    preserve_existing_hosts: True
    preserve_existing_children: True

```

For more examples and information on options see
[`awx.awx.group`](https://docs.ansible.com/ansible/latest/collections/awx/awx/group_module.html).
#### `host`: create, update, or destroy Automation Platform Controller host.

```yaml
# Example using awx_host variable
- name: Include tasks for AWX hosts
  ansible.builtin.include_tasks: host.yml
  loop: "{{ awx_host }}"
  loop_control:
    loop_var: awx_resource_item

# Example using awx_resources variable
- name: Include tasks for generic AWX resources
  ansible.builtin.include_tasks: >-
    {{ awx_resource_item['module'] | default(awx_resource_item['type']) }}.yml
  loop: "{{ awx_resources }}"
  loop_control:
    loop_var: awx_resource_item

```

```yaml
tasks/host.yml example using awx_host:
- name: Manage AWX Hosts
  awx.awx.host:
    controller_config_file: "{{ awx_resource_item.controller_config_file | default(omit) }}"
    controller_host: "{{ awx_resource_item.controller_host | default(omit) }}"
    controller_oauthtoken: "{{ awx_resource_item.controller_oauthtoken | default(omit) }}"
    controller_password: "{{ awx_resource_item.controller_password | default(omit) }}"
    controller_username: "{{ awx_resource_item.controller_username | default(omit) }}"
    description: "{{ awx_resource_item.description | default(omit) }}"
    enabled: "{{ awx_resource_item.enabled | default(omit) }}"
    inventory: "{{ awx_resource_item.inventory }}"
    name: "{{ awx_resource_item.name }}"
    new_name: "{{ awx_resource_item.new_name | default(omit) }}"
    request_timeout: "{{ awx_resource_item.request_timeout | default(omit) }}"
    state: "{{ awx_resource_item.state | default('present') }}"
    validate_certs: "{{ awx_resource_item.validate_certs | default(omit) }}"
    variables: "{{ awx_resource_item.variables | default(omit) }}"

```

```yaml
tasks/host.yml example using awx_resources:
- name: Manage AWX Hosts
  awx.awx.host:
    controller_config_file: "{{ awx_resource_item['controller_config_file'] | default(omit) }}"
    controller_host: "{{ awx_resource_item['controller_host'] | default(omit) }}"
    controller_oauthtoken: "{{ awx_resource_item['controller_oauthtoken'] | default(omit) }}"
    controller_password: "{{ awx_resource_item['controller_password'] | default(omit) }}"
    controller_username: "{{ awx_resource_item['controller_username'] | default(omit) }}"
    description: "{{ awx_resource_item['description'] | default(omit) }}"
    enabled: "{{ awx_resource_item['enabled'] | default(omit) }}"
    inventory: "{{ awx_resource_item['inventory'] }}"
    name: "{{ awx_resource_item['name'] }}"
    new_name: "{{ awx_resource_item['new_name'] | default(omit) }}"
    request_timeout: "{{ awx_resource_item['request_timeout'] | default(omit) }}"
    state: "{{ awx_resource_item['state'] | default('present') }}"
    validate_certs: "{{ awx_resource_item['validate_certs'] | default(omit) }}"
    variables: "{{ awx_resource_item['variables'] | default(omit) }}"

```

For more examples and information on options see
[`awx.awx.host`](https://docs.ansible.com/ansible/latest/collections/awx/awx/host_module.html).
#### `import`: import resources into Automation Platform Controller.

```yaml
awx_import:
  - assets: "{{ export_output.assets }}"
  - assets: "{{ lookup('file', 'org.json') | from_json() }}"

```

```yaml
awx_resources:
  - module: import
    assets: "{{ export_output.assets }}"
  - module: import
    assets: "{{ lookup('file', 'org.json') | from_json() }}"

```

For more examples and information on options see
[`awx.awx.import`](https://docs.ansible.com/ansible/latest/collections/awx/awx/import_module.html).
#### `instance`: create, update, or destroy Automation Platform Controller instances.

```yaml
awx_instance:
  - hostname: my-instance.prod.example.com
    capacity_adjustment: 0.4
    listener_port: 31337
  - hostname: another-instance.prod.example.com
    capacity_adjustment: 0.7
    enabled: true

```

```yaml
awx_resources:
  - module: instance
    hostname: my-instance.prod.example.com
    capacity_adjustment: 0.4
    listener_port: 31337
  - module: instance
    hostname: another-instance.prod.example.com
    capacity_adjustment: 0.7
    enabled: true

```

For more examples and information on options see
[`awx.awx.instance`](https://docs.ansible.com/ansible/latest/collections/awx/awx/instance_module.html).
#### `instance_group`: create, update, or destroy Automation Platform Controller instance groups.

```yaml
awx_instance_group:
  - name: example_instance_group
    credential: kube_credential
    instances:
      - instance1
      - instance2
    is_container_group: true
    max_concurrent_jobs: 10
    max_forks: 20
    state: present

```

```yaml
awx_resources:
  - module: instance_group
    name: example_instance_group
    credential: kube_credential
    instances:
      - instance1
      - instance2
    is_container_group: true
    max_concurrent_jobs: 10
    max_forks: 20
    state: present

```

For more examples and information on options see
[`awx.awx.instance_group`](https://docs.ansible.com/ansible/latest/collections/awx/awx/instance_group_module.html).
#### `inventory`: create, update, or destroy Automation Platform Controller inventory.

```yaml
awx_inventory:
  - name: "Foo Inventory"
    description: "Our Foo Cloud Servers"
    organization: "Bar Org"
    state: present
    controller_config_file: "~/tower_cli.cfg"
  - name: "Copy Foo Inventory"
    copy_from: "Default Inventory"
    description: "Our Foo Cloud Servers"
    organization: "Foo"
    state: present
  - name: "My Constructed Inventory"
    organization: "Default"
    kind: "constructed"
    input_inventories:
      - "West Datacenter"
      - "East Datacenter"

```

```yaml
awx_resources:
  - module: inventory
    name: "Foo Inventory"
    description: "Our Foo Cloud Servers"
    organization: "Bar Org"
    state: present
    controller_config_file: "~/tower_cli.cfg"
  - module: inventory
    name: "Copy Foo Inventory"
    copy_from: "Default Inventory"
    description: "Our Foo Cloud Servers"
    organization: "Foo"
    state: present
  - module: inventory
    name: "My Constructed Inventory"
    organization: "Default"
    kind: "constructed"
    input_inventories:
      - "West Datacenter"
      - "East Datacenter"

```

For more examples and information on options see
[`awx.awx.inventory`](https://docs.ansible.com/ansible/latest/collections/awx/awx/inventory_module.html).
#### `inventory_source`: create, update, or destroy Automation Platform Controller inventory source.

```yaml
awx_inventory_source:
  - name: source-inventory
    description: "Source for inventory"
    inventory: previously-created-inventory
    credential: previously-created-credential
    overwrite: true
    update_on_launch: true
    organization: Default
    source_vars:
      private: false

```

```yaml
awx_resources:
  - module: inventory_source
    name: source-inventory
    description: "Source for inventory"
    inventory: previously-created-inventory
    credential: previously-created-credential
    overwrite: true
    update_on_launch: true
    organization: Default
    source_vars:
      private: false

```

For more examples and information on options see
[`awx.awx.inventory_source`](https://docs.ansible.com/ansible/latest/collections/awx/awx/inventory_source_module.html).
#### `inventory_source_update`: Update inventory source(s).

```yaml
# Example using awx_inventory_source_update
awx_inventory_source_update:
  - name: "Example Inventory Source"
    inventory: "My Inventory"
    organization: "Default"
    controller_username: "admin"
    controller_password: "password"
    controller_host: "https://awx.example.com"
    wait: true

```

```yaml
# Generic awx_resources variable with an inventory_source_update entry
awx_resources:
  - module: inventory_source_update
    name: "All Inventory Sources"
    inventory: "My Other Inventory"
    controller_username: "admin"
    controller_password: "password"
    controller_host: "https://awx.example.com"
    wait: true

```

For more examples and information on options see
[`awx.awx.inventory_source_update`](https://docs.ansible.com/ansible/latest/collections/awx/awx/inventory_source_update_module.html).
#### `job_cancel`: Cancel an Automation Platform Controller Job.

```yaml
# Example using awx_job_cancel variable
awx_job_cancel:
  - job_id: 1234
    controller_host: "controller.example.com"
    controller_username: "admin"
    controller_password: "password"
    validate_certs: false

```

```yaml
# Example using awx_resources variable
awx_resources:
  - module: job_cancel
    job_id: 5678
    controller_host: "controller.example.com"
    controller_username: "admin"
    controller_password: "password"
    validate_certs: true

```

For more examples and information on options see
[`awx.awx.job_cancel`](https://docs.ansible.com/ansible/latest/collections/awx/awx/job_cancel_module.html).
#### `job_launch`: Launch an Ansible Job.

```yaml
awx_job_launch:
  - name: "My Job Template"
    extra_vars:
      var1: "My First Variable"
      var2: "My Second Variable"
    job_type: "run"
    controller_host: "https://your-controller-instance"
    controller_username: "admin"
    controller_password: "admin_password"

```

```yaml
awx_resources:
  - module: job_launch
    name: "My Job Template"
    extra_vars:
      var1: "My First Variable"
      var2: "My Second Variable"
    job_type: "run"
    controller_host: "https://your-controller-instance"
    controller_username: "admin"
    controller_password: "admin_password"

```

For more examples and information on options see
[`awx.awx.job_launch`](https://docs.ansible.com/ansible/latest/collections/awx/awx/job_launch_module.html).
#### `job_list`: List Automation Platform Controller jobs.

```yaml
# Example of using awx_job_list variable
awx_job_list:
  - status: running
    query: {"playbook": "testing.yml"}
    controller_config_file: "~/tower_cli.cfg"
  - status: successful
    page: 1
    controller_host: "https://controller.example.com"
    controller_username: "admin"
    controller_password: "password"

```

```yaml
# Example of using awx_resources variable containing job_list module
awx_resources:
  - module: job_list
    status: running
    query: {"project": "my_project"}
    controller_oauthtoken: "bqV5txm97wqJqtkxlMkhQz0pKhRMMX"
  - module: job_list
    status: pending
    validate_certs: no
    controller_host: "https://controller.example.com"
    controller_username: "admin"
    controller_password: "password"

```

For more examples and information on options see
[`awx.awx.job_list`](https://docs.ansible.com/ansible/latest/collections/awx/awx/job_list_module.html).
#### `job_template`: create, update, or destroy Automation Platform Controller job templates.

```yaml
awx_job_template:
  - name: "Ping"
    job_type: "run"
    organization: "Default"
    inventory: "Local"
    project: "Demo"
    playbook: "ping.yml"
    credentials:
      - "Local"
      - "2nd credential"
    state: "present"
    controller_config_file: "~/tower_cli.cfg"
    survey_enabled: yes
    survey_spec: "{{ lookup('file', 'my_survey.json') }}"
  - name: "Add Notification"
    job_type: "run"
    organization: "Default"
    inventory: "Local"
    project: "Demo"
    playbook: "ping.yml"
    credentials:
      - "Local"
    state: "present"
    notification_templates_started:
      - "Notification1"
      - "Notification2"

```

```yaml
awx_resources:
  - module: job_template
    name: "Ping"
    job_type: "run"
    organization: "Default"
    inventory: "Local"
    project: "Demo"
    playbook: "ping.yml"
    credentials:
      - "Local"
      - "2nd credential"
    state: "present"
    controller_config_file: "~/tower_cli.cfg"
    survey_enabled: yes
    survey_spec: "{{ lookup('file', 'my_survey.json') }}"
  - module: job_template
    name: "Ping"
    notification_templates_started:
      - "Notification2"
    state: "absent"

```

For more examples and information on options see
[`awx.awx.job_template`](https://docs.ansible.com/ansible/latest/collections/awx/awx/job_template_module.html).
#### `job_wait`: Wait for Automation Platform Controller job to finish.

```yaml
awx_job_wait:
  - job_id: 123
    timeout: 120
    controller_host: "https://awx.example.com"
    controller_username: "admin"
    controller_password: "secret"

```

```yaml
awx_resources:
  - module: job_wait
    job_id: 456
    timeout: 180
    controller_host: "https://awx.example.com"
    controller_oauthtoken: "bqV5txm97wqJqtkxlMkhQz0pKhRMMX"

```

For more examples and information on options see
[`awx.awx.job_wait`](https://docs.ansible.com/ansible/latest/collections/awx/awx/job_wait_module.html).
#### `label`: create, update, or destroy Automation Platform Controller labels.

```yaml
# Example of using awx_label variable directly
awx_label:
  - name: Custom Label
    organization: My Organization
    controller_host: https://example.com
    controller_username: admin
    controller_password: password
    validate_certs: no
  - name: Another Label
    organization: Another Organization
    controller_oauthtoken: bqV5txm97wqJqtkxlMkhQz0pKhRMMX
    state: present

```

```yaml
# Example of using awx_resources with label module
awx_resources:
  - module: label
    name: Resource Label
    organization: Resource Organization
    controller_host: https://example.com
    controller_username: admin
    controller_password: password
    validate_certs: no
  - module: label
    name: Another Resource Label
    organization: Another Resource Organization
    controller_oauthtoken: bqV5txm97wqJqtkxlMkhQz0pKhRMMX
    state: present

```

For more examples and information on options see
[`awx.awx.label`](https://docs.ansible.com/ansible/latest/collections/awx/awx/label_module.html).
#### `license`: Set the license for Automation Platform Controller

```yaml
# Example using awx_license
awx_license:
  - controller_host: "https://my-controller-instance.com"
    controller_username: "admin"
    controller_password: "password"
    manifest: "/tmp/my_manifest.zip"
    state: present
  - controller_host: "https://my-controller-instance.com"
    controller_username: "admin"
    controller_password: "password"
    pool_id: "123456"
    state: present
  - controller_host: "https://my-controller-instance.com"
    controller_username: "admin"
    controller_password: "password"
    state: absent

- name: Apply AWX license using awx_license variable
  ansible.builtin.include_tasks: license.yml
  loop: "{{ awx_license }}"
  loop_control:
    loop_var: awx_resource_item

```

```yaml
# Example using awx_resources
awx_resources:
  - module: license
    controller_host: "https://my-controller-instance.com"
    controller_username: "admin"
    controller_password: "password"
    manifest: "/tmp/my_manifest.zip"
    state: present
  - module: license
    controller_host: "https://my-controller-instance.com"
    controller_username: "admin"
    controller_password: "password"
    pool_id: "123456"
    state: present
  - module: license
    controller_host: "https://my-controller-instance.com"
    controller_username: "admin"
    controller_password: "password"
    state: absent

- name: Apply AWX license using awx_resources variable
  ansible.builtin.include_tasks: >-
    {{ awx_resource_item['module'] | default(awx_resource_item['type']) }}.yml
  with_items: >-
    {{ awx_resources
    | c2platform.core.groups2items('resource_group') }}
  loop_control:
    loop_var: awx_resource_item

```

For more examples and information on options see
[`awx.awx.license`](https://docs.ansible.com/ansible/latest/collections/awx/awx/license_module.html).
#### `notification_template`: create, update, or destroy Automation Platform Controller notification.

```yaml
awx_notification_template:
  - name: slack_notification
    organization: Default
    notification_type: slack
    notification_configuration:
      channels:
        - general
      token: cefda9e2be1f21d11cdd9452f5b7f97fda977f42
    messages:
      started:
        message: "{{ '{{ job_friendly_name }}{{ job.id }} started' }}"
      success:
        message: "{{ '{{ job_friendly_name }} completed in {{ job.elapsed }} seconds' }}"
      error:
        message: "{{ '{{ job_friendly_name }} FAILED! Please look at {{ job.url }}' }}"
    state: present
    controller_config_file: "~/tower_cli.cfg"

  - name: email_notification
    notification_type: email
    notification_configuration:
      username: user
      password: s3cr3t
      sender: controller@example.com
      recipients:
        - user1@example.com
      host: smtp.example.com
      port: 25
      use_tls: no
      use_ssl: no
    state: present
    controller_config_file: "~/tower_cli.cfg"

```

```yaml
awx_resources:
  - module: notification_template
    name: webhook_notification
    notification_type: webhook
    notification_configuration:
      url: http://www.example.com/hook
      headers:
        X-Custom-Header: value123
    state: present
    controller_config_file: "~/tower_cli.cfg"

  - module: notification_template
    name: irc_notification
    notification_type: irc
    notification_configuration:
      nickname: controller
      password: s3cr3t
      targets:
        - user1
      port: 8080
      server: irc.example.com
      use_ssl: no
    state: present
    controller_config_file: "~/tower_cli.cfg"

```

For more examples and information on options see
[`awx.awx.notification_template`](https://docs.ansible.com/ansible/latest/collections/awx/awx/notification_template_module.html).
#### `organization`: create, update, or destroy Automation Platform Controller organizations

```yaml
awx_organization:
  - name: "Foo"
    description: "Foo bar organization"
    state: present
    controller_config_file: "~/tower_cli.cfg"
  - name: "Bar"
    description: "Bar bar organization"
    state: present
    controller_config_file: "~/tower_cli.cfg"

```

```yaml
awx_resources:
  - module: organization
    name: "Foo"
    description: "Foo bar organization using foo-venv"
    state: present
    controller_config_file: "~/tower_cli.cfg"
    custom_virtualenv: "foo-venv"
  - module: organization
    name: "Bar"
    description: "Bar bar organization that pulls content from galaxy.ansible.com"
    state: present
    galaxy_credentials:
      - "Ansible Galaxy"
    controller_config_file: "~/tower_cli.cfg"

```

For more examples and information on options see
[`awx.awx.organization`](https://docs.ansible.com/ansible/latest/collections/awx/awx/organization_module.html).
#### `project`: create, update, or destroy Automation Platform Controller projects

```yaml
# Example using awx_project variable
awx_project:
  - name: "Foo"
    description: "Foo bar project"
    organization: "test"
    state: present
    controller_config_file: "~/tower_cli.cfg"
  - name: "Bar"
    description: "Bar project"
    organization: "example"
    scm_update_on_launch: true
    scm_update_cache_timeout: 60
    state: present
    controller_config_file: "~/tower_cli.cfg"

```

```yaml
# Example using awx_resources variable
awx_resources:
  - module: project
    name: "Baz"
    description: "Baz copy project"
    organization: "Foo"
    copy_from: "test"
    state: present
    controller_config_file: "~/tower_cli.cfg"
  - module: project
    name: "Qux"
    description: "Qux bar project"
    organization: "Bar"
    scm_update_on_launch: true
    scm_update_cache_timeout: 120
    state: present
    controller_config_file: "~/tower_cli.cfg"

```

For more examples and information on options see
[`awx.awx.project`](https://docs.ansible.com/ansible/latest/collections/awx/awx/project_module.html).
#### `project_update`: Update a Project in Automation Platform Controller

```yaml
awx_project_update:
  - name: "Networking Project"
    timeout: 10
  - name: "Other Project"
    wait: False

```

```yaml
awx_resources:
  - module: project_update
    name: "Networking Project"
    timeout: 10
  - module: project_update
    name: "Other Project"
    wait: False

```

For more examples and information on options see
[`awx.awx.project_update`](https://docs.ansible.com/ansible/latest/collections/awx/awx/project_update_module.html).
#### `role`: grant or revoke an Automation Platform Controller role.

```yaml
awx_role:
  - user: jdoe
    target_team: "My Team"
    role: member
    state: present
  - user: joe
    role: execute
    workflows:
      - test-role-workflow
    job_templates:
      - jt1
      - jt2
    state: present

```

```yaml
awx_resources:
  - module: role
    user: jdoe
    target_team: "My Team"
    role: member
    state: present
  - module: role
    user: joe
    role: execute
    workflows:
      - test-role-workflow
    job_templates:
      - jt1
      - jt2
    state: present

```

For more examples and information on options see
[`awx.awx.role`](https://docs.ansible.com/ansible/latest/collections/awx/awx/role_module.html).
#### `schedule`: create, update, or destroy Automation Platform Controller schedules.

```yaml
awx_schedule:
  - name: "Daily Backup Schedule"
    state: present
    unified_job_template: "Backup Job Template"
    rrule: "DTSTART:20231011T130551Z RRULE:FREQ=DAILY;INTERVAL=1;COUNT=5"
    description: "Daily backup job."
    enabled: true
    extra_data:
      backup_type: "full"
  - name: "Weekly Reporting Schedule"
    state: present
    unified_job_template: "Report Job Template"
    rrule: "{{ query('awx.awx.schedule_rrule', 'week', start_date='2023-10-11 14:00:00') }}"
    description: "Weekly reporting job."
    enabled: true

```

```yaml
awx_resources:
  - module: schedule
    name: "Monthly Maintenance Schedule"
    state: present
    unified_job_template: "Maintenance Job Template"
    rrule: "DTSTART:20231011T160000Z RRULE:FREQ=MONTHLY;INTERVAL=1;COUNT=12"
    description: "Monthly maintenance job."
    enabled: true
    inventory: "Maintenance Inventory"
    job_tags: "maintenance,monthly"
    extra_data:
      maintenance_window: "2 hours"
  - module: schedule
    name: "Quarterly Audit Schedule"
    state: present
    unified_job_template: "Audit Job Template"
    rrule: "{{ query('awx.awx.schedule_rrule', 'quarter', start_date='2023-10-11 10:00:00') }}"
    description: "Quarterly audit job."
    enabled: true

```

For more examples and information on options see
[`awx.awx.schedule`](https://docs.ansible.com/ansible/latest/collections/awx/awx/schedule_module.html).
#### `settings`: Modify Automation Platform Controller settings.

```yaml
awx_settings:
  - name: AWX_ISOLATION_BASE_PATH
    value: "/tmp"
  - name: AWX_ISOLATION_SHOW_PATHS
    value: "'/var/lib/awx/projects/', '/tmp'"

```

```yaml
awx_resources:
  - module: settings
    name: AUTH_LDAP_BIND_PASSWORD
    value: "Password"
  - module: settings
    settings:
      AUTH_LDAP_BIND_PASSWORD: "password"
      AUTH_LDAP_USER_ATTR_MAP:
        email: "mail"
        first_name: "givenName"
        last_name: "surname"

```

For more examples and information on options see
[`awx.awx.settings`](https://docs.ansible.com/ansible/latest/collections/awx/awx/settings_module.html).
#### `subscriptions`: Get subscription list

```yaml
awx_subscriptions:
  - username: "my_username"
    password: "My Password"
  - username: "my_username"
    password: "My Password"
    filters:
      product_name: "Red Hat Ansible Automation Platform"
      support_level: "Self-Support"

```

```yaml
awx_resources:
  - module: subscriptions
    username: "my_username"
    password: "My Password"
  - module: subscriptions
    username: "my_username"
    password: "My Password"
    filters:
      product_name: "Red Hat Ansible Automation Platform"
      support_level: "Self-Support"

```

For more examples and information on options see
[`awx.awx.subscriptions`](https://docs.ansible.com/ansible/latest/collections/awx/awx/subscriptions_module.html).
#### `team`: create, update, or destroy Automation Platform Controller team.

```yaml
awx_team:
  - name: Team Name 1
    description: Team Description 1
    organization: org1
    controller_config_file: ~/tower_cli.cfg
    state: present
  - name: Team Name 2
    description: Team Description 2
    organization: org2
    controller_host: https://controller.example.com
    controller_username: admin
    controller_password: password
    state: present

```

```yaml
awx_resources:
  - module: team
    name: Team Name 3
    description: Team Description 3
    organization: org3
    controller_oauthtoken: bqV5txm97wqJqtkxlMkhQz0pKhRMMX
    validate_certs: no
    state: present
  - module: team
    name: Team Name 4
    organization: org4
    controller_host: https://controller.example.com
    controller_oauthtoken: token_dict
    new_name: New Team Name 4
    state: present

```

For more examples and information on options see
[`awx.awx.team`](https://docs.ansible.com/ansible/latest/collections/awx/awx/team_module.html).
#### `token`: create, update, or destroy Automation Platform Controller tokens.

```yaml
awx_token:
  - description: "{{ token_description }}"
    scope: "write"
    state: present
    controller_oauthtoken: "{{ my_existing_token }}"
  - existing_token: "{{ controller_token }}"
    state: absent
  - description: "{{ token_description }}"
    scope: "write"
    state: present
    controller_username: "{{ my_username }}"
    controller_password: "{{ my_password }}"

```

```yaml
awx_resources:
  - module: token
    description: "{{ token_description }}"
    scope: "write"
    state: present
    controller_oauthtoken: "{{ my_existing_token }}"
  - module: token
    existing_token: "{{ controller_token }}"
    state: absent
  - module: token
    description: "{{ token_description }}"
    scope: "write"
    state: present
    controller_username: "{{ my_username }}"
    controller_password: "{{ my_password }}"

```

For more examples and information on options see
[`awx.awx.token`](https://docs.ansible.com/ansible/latest/collections/awx/awx/token_module.html).
#### `user`: create, update, or destroy Automation Platform Controller users.

```yaml
awx_user:
  - username: jdoe
    password: foobarbaz
    email: jdoe@example.org
    first_name: John
    last_name: Doe
    state: present
    controller_config_file: "~/tower_cli.cfg"
  - username: jdoe_admin
    password: foobarbaz
    email: jdoe@example.org
    first_name: John
    last_name: Doe
    is_superuser: yes
    state: present
    controller_config_file: "~/tower_cli.cfg"
  - username: jdoe_auditor
    password: foobarbaz
    email: jdoe@example.org
    first_name: John
    last_name: Doe
    is_system_auditor: yes
    state: present
    controller_config_file: "~/tower_cli.cfg"
  - username: jdoe_org
    password: foobarbaz
    email: jdoe@example.org
    first_name: John
    last_name: Doe
    organization: devopsorg
    state: present
  - username: jdoe
    email: jdoe@example.org
    state: absent
    controller_config_file: "~/tower_cli.cfg"

```

```yaml
awx_resources:
  - module: user
    username: jdoe
    password: foobarbaz
    email: jdoe@example.org
    first_name: John
    last_name: Doe
    state: present
    controller_config_file: "~/tower_cli.cfg"
  - module: user
    username: jdoe_admin
    password: foobarbaz
    email: jdoe@example.org
    first_name: John
    last_name: Doe
    is_superuser: yes
    state: present
    controller_config_file: "~/tower_cli.cfg"
  - module: user
    username: jdoe_auditor
    password: foobarbaz
    email: jdoe@example.org
    first_name: John
    last_name: Doe
    is_system_auditor: yes
    state: present
    controller_config_file: "~/tower_cli.cfg"
  - module: user
    username: jdoe_org
    password: foobarbaz
    email: jdoe@example.org
    first_name: John
    last_name: Doe
    organization: devopsorg
    state: present
  - module: user
    username: jdoe
    email: jdoe@example.org
    state: absent
    controller_config_file: "~/tower_cli.cfg"

```

For more examples and information on options see
[`awx.awx.user`](https://docs.ansible.com/ansible/latest/collections/awx/awx/user_module.html).
#### `workflow_approval`: Approve an approval node in a workflow job.

```yaml
# Using awx_workflow_approval variable
awx_workflow_approval:
  - name: "approval_jt_name"
    workflow_job_id: 1234
    interval: 10
    timeout: 20
    action: "approve"
  - name: "another_approval_node"
    workflow_job_id: 5678
    interval: 5
    timeout: 15
    action: "deny"

```

```yaml
# Using awx_resources variable
awx_resources:
  - module: workflow_approval
    name: "approval_jt_name"
    workflow_job_id: 1234
    interval: 10
    timeout: 20
    action: "approve"
  - module: workflow_approval
    name: "another_approval_node"
    workflow_job_id: 5678
    interval: 5
    timeout: 15
    action: "deny"

```

For more examples and information on options see
[`awx.awx.workflow_approval`](https://docs.ansible.com/ansible/latest/collections/awx/awx/workflow_approval_module.html).
#### `workflow_job_template`: create, update, or destroy Automation Platform Controller workflow job templates.

```yaml
awx_workflow_job_template:
  - name: example-workflow
    description: created by Ansible Playbook
    organization: Default
    state: present
    inventory: Demo Inventory
    extra_vars:
      foo: bar
      another-foo:
        barz: bar2
    workflow_nodes:
      - identifier: node101
        unified_job_template:
          name: example-project
          inventory:
            organization:
              name: Default
          type: inventory_source
        related:
          success_nodes: []
          failure_nodes:
            - identifier: node201
          always_nodes: []
          credentials:
            - local_cred
            - supplementary_cred
      - identifier: node201
        unified_job_template:
          name: job template 1
          organization:
            name: Default
          type: job_template
        related:
          success_nodes:
            - identifier: node301
          failure_nodes: []
          always_nodes: []
          credentials: []

```

```yaml
awx_resources:
  - module: workflow_job_template
    name: copy-workflow
    copy_from: example-workflow
    organization: Foo
    state: present

```

For more examples and information on options see
[`awx.awx.workflow_job_template`](https://docs.ansible.com/ansible/latest/collections/awx/awx/workflow_job_template_module.html).
#### `workflow_job_template_node`: create, update, or destroy Automation Platform Controller workflow job template nodes.

```yaml
# Example using awx_workflow_job_template_node
awx_workflow_job_template_node:
  - identifier: my-first-node
    workflow_job_template: example-workflow
    unified_job_template: jt-for-node-use
    organization: Default
    extra_data:
      foo_key: bar_value
  - identifier: my-root-node
    workflow_job_template: example-workflow
    unified_job_template: jt-for-node-use
    organization: Default
    success_nodes:
      - my-first-node
  - identifier: my-first-node
    workflow_job_template: my-workflow-job-template
    unified_job_template: some_job_template
    organization: Default
  - identifier: my-second-approval-node
    workflow_job_template: my-workflow-job-template
    organization: Default
    approval_node:
      description: "Do this?"
      name: my-second-approval-node
      timeout: 3600
  - identifier: my-third-node
    workflow_job_template: my-workflow-job-template
    unified_job_template: some_other_job_template
    organization: Default
  - identifier: my-first-node
    workflow_job_template: my-workflow-job-template
    organization: Default
    success_nodes:
      - my-second-approval-node
  - identifier: my-second-approval-node
    workflow_job_template: my-workflow-job-template
    organization: Default
    success_nodes:
      - my-third-node

```

```yaml
# Example using awx_resources
awx_resources:
  - module: workflow_job_template_node
    identifier: my-first-node
    workflow_job_template: example-workflow
    unified_job_template: jt-for-node-use
    organization: Default
    extra_data:
      foo_key: bar_value
  - module: workflow_job_template_node
    identifier: my-root-node
    workflow_job_template: example-workflow
    unified_job_template: jt-for-node-use
    organization: Default
    success_nodes:
      - my-first-node
  - module: workflow_job_template_node
    identifier: my-first-node
    workflow_job_template: my-workflow-job-template
    unified_job_template: some_job_template
    organization: Default
  - module: workflow_job_template_node
    identifier: my-second-approval-node
    workflow_job_template: my-workflow-job-template
    organization: Default
    approval_node:
      description: "Do this?"
      name: my-second-approval-node
      timeout: 3600
  - module: workflow_job_template_node
    identifier: my-third-node
    workflow_job_template: my-workflow-job-template
    unified_job_template: some_other_job_template
    organization: Default
  - module: workflow_job_template_node
    identifier: my-first-node
    workflow_job_template: my-workflow-job-template
    organization: Default
    success_nodes:
      - my-second-approval-node
  - module: workflow_job_template_node
    identifier: my-second-approval-node
    workflow_job_template: my-workflow-job-template
    organization: Default
    success_nodes:
      - my-third-node

```

For more examples and information on options see
[`awx.awx.workflow_job_template_node`](https://docs.ansible.com/ansible/latest/collections/awx/awx/workflow_job_template_node_module.html).
#### `workflow_launch`: Run a workflow in Automation Platform Controller

```yaml
# This example uses the awx_workflow_launch variable
awx_workflow_launch:
    - name: "Test Workflow"
      timeout: 10
    - name: "Test workflow"
      extra_vars:
        var1: "My First Variable"
        var2: "My Second Variable"
      wait: False

```

```yaml
# This example uses the awx_resources variable (workflow_launch module)
awx_resources:
    - module: workflow_launch
      name: "Test Workflow"
      timeout: 10
    - module: workflow_launch
      name: "Test workflow"
      extra_vars:
        var1: "My First Variable"
        var2: "My Second Variable"
      wait: False

```

For more examples and information on options see
[`awx.awx.workflow_launch`](https://docs.ansible.com/ansible/latest/collections/awx/awx/workflow_launch_module.html).
#### `workflow_node_wait`: Wait for a workflow node to finish.

```yaml
awx_workflow_node_wait:
  - name: Approval Data Step
    workflow_job_id: "{{ workflow.id }}"
    timeout: 120
  - name: Another Workflow Step
    workflow_job_id: "{{ another_workflow.id }}"
    interval: 2.0
    validate_certs: false

```

```yaml
awx_resources:
  - module: workflow_node_wait
    name: Approval Data Step
    workflow_job_id: "{{ workflow.id }}"
    timeout: 120
  - module: workflow_node_wait
    name: Another Workflow Step
    workflow_job_id: "{{ another_workflow.id }}"
    interval: 2.0
    validate_certs: false

```

For more examples and information on options see
[`awx.awx.workflow_node_wait`](https://docs.ansible.com/ansible/latest/collections/awx/awx/workflow_node_wait_module.html).
<!-- end supported_modules -->

## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

## Example Playbook

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

```yaml
---
- name: awx.yml
  hosts: awx
  become: yes

  roles:
    - { role: c2platform.core.common, tags: ["common"] }
    - { role: c2platform.mw.microk8s, tags: ["kubernetes", "microk8s"] }
    - { role: c2platform.mw.kubernetes, tags: ["kubernetes"] }
    - { role: c2platform.mgmt.awx, tags: ["awx"] }
```

See [Setup AWX | C2 Platform](https://c2platform.org/en/docs/howto/awx/awx/) for more information.
# Ansible Role Harbor

An Ansible Role that installs [Harbor](https://goharbor.io/) using the "online installer" see [Harbor docs | Harbor Installation and Configuration](https://goharbor.io/docs/2.6.0/install-config/)

- [Requirements](#requirements)
- [Role Variables](#role-variables)
  - [Certificates](#certificates)
  - [Files, directories and ACL](#files-directories-and-acl)
  - [Harbor configuration](#harbor-configuration)
- [Dependencies](#dependencies)
- [Example Playbook](#example-playbook)


## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

### Certificates

If you want to use SSL / TLS you can use dict `harbor_cacerts2_certificates` in combination with [c2platform.core.cacerts2](https://gitlab.com/c2platform/ansible-collection-core/-/tree/master/roles/cacerts2).

### Files, directories and ACL

Use dicts `apache_files`, `apache_directories`, `apache_acl` to create / manage any other files, directories and ACL. See [c2platform.core.files](https://gitlab.com/c2platform/ansible-collection-core/-/tree/master/roles/files).

### Harbor configuration

Harbor uses a file `harbor.yml` for configuration. You can use dictionary `harbor_files` to create this file as shown below. Note use of a helper variable / dictionary `project1_harbor_configuration`. Check the file `harbor.yml.tmpl` in `harbor_home` for all the configuration options.

```yaml
harbor_files:
  configuration:
    - content: "{{ project1_harbor_configuration | to_nice_yaml }}"
      dest: "{{ harbor_home}}/harbor.yml"
```

```yaml
project1_harbor_configuration:
  hostname: "{{ harbor_hostname }}"
  certificate: "{{ harbor_cacerts2_certificates[0]['deploy']['crt']['dest'] }}"
  private_key: "{{ harbor_cacerts2_certificates[0]['deploy']['key']['dest'] }}"
  external_url: "https://{{ harbor_hostname }}"
  harbor_admin_password: "{{ harbor_admin_password }}"
  database:
    password: "{{ harbor_database_password }}"
    max_idle_conns: 100
    max_open_conns: 900
  data_volume: "{{ harbor_home }}/data"
  trivy:
    ignore_unfixed: false
    skip_update: false
    offline_scan: false
    insecure: false
  jobservice:
    max_job_workers: 10
  notification:
    webhook_job_max_retry: 10
  chart:
    absolute_url: disabled
  log:
    level: "{{ harbor_log_level }}"
    local:
      rotate_count: 50
      rotate_size: 200M
      location: /var/log/harbor
  _version: 2.6.0
  uaa:
    ca_file: "/etc/docker/certs.d/{{ harbor_hostname }}/ca.crt"
  proxy:
    http_proxy:
    https_proxy:
    no_proxy:
    components: []
  upload_purging:
    enabled: true
    age: 168h
    interval: 24h
    dryrun: false
  cache:
    enabled: false
    expire_hours: 24
```

## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

## Example Playbook

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

```yaml
    - hosts: harbor
      roles:
         - { role: c2platform.mgmt.harbor }

      vars:
        harbor_hostname: registry.example.com
        harbor_log_level: debug  # debug, info, warning, error, fatal # TODO
        harbor_cacerts2_certificates:
          - common_name: registry
            subject_alt_name:
            - "DNS:{{ harbor_hostname }}"
            - "DNS:{{ ok_apache_server_name }}"
            - "DNS:{{ ansible_hostname }}"
            - "DNS:{{ ansible_fqdn }}"
            - "IP:{{ ansible_eth1.ipv4.address }}"
            ansible_group: haproxy
            deploy:
              key:
                dir: "/etc/docker/certs.d/{{ harbor_hostname }}"
                dest: "/etc/docker/certs.d/{{ harbor_hostname }}/{{ harbor_hostname }}.key"
                mode: '640'
              crt:
                dir: "/etc/docker/certs.d/{{ harbor_hostname }}"
                dest: "/etc/docker/certs.d/{{ harbor_hostname }}/{{ harbor_hostname }}.cert"
                mode: '644'
            deploy_ca:
              crt:
                dir: "/etc/docker/certs.d/{{ harbor_hostname }}"
                dest: "/etc/docker/certs.d/{{ harbor_hostname }}/ca.crt"
                mode: '644'

        harbor_files:
          configuration:
            - content: "{{ project1_harbor_configuration | to_nice_yaml }}"
              dest: "{{ harbor_home}}/harbor.yml"
```
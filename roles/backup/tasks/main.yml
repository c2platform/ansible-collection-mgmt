---
- name: Set fact backup_roles_node
  ansible.builtin.set_fact:
    backup_roles_node: >-
      {{ backup_roles_supported
      | intersect(ansible_play_role_names)
      | map('regex_replace', '^(.*)\.(.*)$', '\2')
      | list }}

- name: Check backup installed
  ansible.builtin.stat:
    path: "/opt/backup/bin"
  register: backup_bin

- name: Setup OS
  ansible.builtin.include_tasks: "setup-{{ ansible_os_family }}.yml"

- name: Create backup dirs
  ansible.builtin.file:
    path: "{{ item }}"
    state: directory
    mode: 0700
  with_items:
    - /etc/backup
    - /etc/backup/models
    - /etc/backup/scripts
    - /etc/backup/hooks

- name: Generate example config file
  ansible.builtin.command: /opt/backup/bin/backup generate:config --config-file=/etc/backup/config-example.rb
  args:
    creates: /etc/backup/config-example.rb

- name: Config rb
  ansible.builtin.template:
    src: config.rb.j2
    dest: /etc/backup/config.rb
    mode: 0644

- name: Models
  ansible.builtin.include_tasks: "models.yml"
- name: Rsnapshot
  ansible.builtin.include_tasks: "rsnapshot.yml"
- name: Alias
  ansible.builtin.include_tasks: "alias.yml"

- name: Cron
  ansible.builtin.template:
    dest: '/etc/cron.d/backup-{{ item }}'
    src: cron.j2
    mode: 0644
  when: backup['schedules'][item] is defined and backup_cron_jobs
  with_items: "{{ backup['schedules_default'] }}"

- name: Disable cron jobs
  ansible.builtin.file:
    path: '/etc/cron.d/backup-{{ item }}'
    state: absent
  when: backup['schedules'][item] is not defined or not backup_cron_jobs
  with_items: "{{ backup['schedules_default'] }}"

- name: Daily, weekly etc scripts
  ansible.builtin.template:
    dest: '/etc/backup/scripts/backup_{{ item }}.sh'
    src: backup.sh.j2
    mode: 0755
  when: backup['schedules'][item] is defined
  with_items: "{{ backup['schedules_default'] }}"

- name: Sql scripts
  ansible.builtin.template:
    dest: '/etc/backup/scripts/{{ item }}'
    src: "{{ item }}.j2"
    mode: 0644
  with_items:
    - "db_snapshot.sql"
    - "db_snapshot_drop.sql"

- name: Packages
  ansible.builtin.package:
    name: "{{ item }}"
    state: present
  with_items:
    - rsync
    - rsnapshot

- name: Restore facts
  c2platform.mgmt.restore_info:
    backup_archives: "{{ backup_archives }}"
    backup_snapshots: "{{ backup_snapshots }}"
    role: "{{ item }}"
    path_pattern: "{{ backup_restore[item]['path_pattern'] }}"
    force: "{{ backup_restore[item]['force'] | default(False) }}"
    database: "{{ vars[item + '_database_name'] is defined }}"
    database_type: "{{ vars[item + '_database_type'] | default('postgresql') }}"
    home: "{{ vars[item + '_home'] }}"
    home_version: "{{ vars[item + '_home_version'] | default(omit) }}"
    home_backup_version: "{{ vars[item + '_home_backup_version'] | default(omit) }}"
    incremental: "{{ vars['backup_roles'][item]['incremental'] | default(false) }}"
    tmp: "{{ backup_tmp }}/{{ inventory_hostname }}-{{ item }}"
    folder: "{{ backup_restore[item]['folder'] | default('home') }}"
    remove_folder: "{{ backup_restore[item]['remove_folder'] | default(True) }}"
    backup_rsnapshot_id_file: "{{ backup_rsnapshot_id_file }}"
    backup_rsnapshot_backup_db_folder: "{{ backup_rsnapshot_backup_db_folder }}"
    backup_name_underscored: "{{ item | c2platform.mgmt.backup_name_underscored(vars) }}"  # no schedule
    backup_restore_home: "{{ backup_restore_home | default(omit) }}"
  with_items: "{{ backup_restore | intersect(backup_roles_node) }}"
  when: backup_restore is defined

- name: Restore facts to /tmp/backup_restore.yml
  ansible.builtin.copy:
    content: "{{ {'backup_restore': backup_restore} | to_nice_yaml }}"
    dest: /tmp/backup_restore.yml
    mode: '664'
  when: backup_restore is defined

- name: Restore block
  when:
    - backup_restore is defined
    - not backup_restore_dry_run | default(False)
  block:
    - name: Restore include tasks
      ansible.builtin.include_tasks: "restore.yml"
      with_items: "{{ backup_restore | intersect(backup_roles_node) }}"
      loop_control:
        loop_var: backup_restore_item
      when: not backup_restore[backup_restore_item]['restored']

    - name: Delete restore file
      ansible.builtin.file:
        path: "/tmp/RESTORE"
        state: absent

    - name: Restored message
      ansible.builtin.debug:
        msg: Backup is already restored
      with_items: "{{ backup_restore | intersect(backup_roles_node) }}"
      loop_control:
        loop_var: backup_restore_item
      when: backup_restore[backup_restore_item]['restored']

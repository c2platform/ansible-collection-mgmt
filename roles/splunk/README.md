# Ansible Role c2platform.mgmt.splunk

This role extends the role
[`mason_splunk.ansible_role_for_splunk.splunk`](https://galaxy.ansible.com/ui/repo/published/mason_splunk/ansible_role_for_splunk/).
It adds the role `c2platform.core.linux` to this Splunk role so that can be used
to configure Splunk.

- [Requirements](#requirements)
- [Role Variables](#role-variables)
  - [`splunk_resources`](#splunk_resources)
- [Dependencies](#dependencies)
- [Example Playbook](#example-playbook)

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

### `splunk_resources`

The role `c2platform.core.linux` is used to process the variable
`splunk_resources` which gives generic and flexible access to more than 115
Ansible modules which then can be used to configure Splunk server of Universal
Forwarder nodes using those modules.

For example we can change the `web.conf` and add a section for accessing Splunk
through a web proxy using `tools.proxy.on = True`.

```yaml
splunk_resources:
  - name: web.conf
    type: copy
    dest: "{{ splunk_home }}/etc/system/local/web.conf"
    owner: "{{ splunk_nix_user }}"
    group: "{{ splunk_nix_group }}"
    backup: true
    content: |
      [expose:tlPackage-scimGroup]
      methods = GET
      pattern = /identity/provisioning/v1/scim/v2/Groups/*

      [expose:tlPackage-scimGroups]
      methods = GET
      pattern = /identity/provisioning/v1/scim/v2/Groups

      [expose:tlPackage-scimUser]
      methods = GET,PUT,PATCH,DELETE
      pattern = /identity/provisioning/v1/scim/v2/Users/*

      [expose:tlPackage-scimUsers]
      methods = GET
      pattern = /identity/provisioning/v1/scim/v2/Users

      [settings]
      root_endpoint = /
      tools.proxy.on = True
      # enableSplunkWebSSL = 1
    notify: restart splunk
```

## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

- [`mason_splunk.ansible_role_for_splunk.splunk`](https://galaxy.ansible.com/ui/repo/published/mason_splunk/ansible_role_for_splunk/)
- [`c2platform.core.linux`](https://galaxy.ansible.com/ui/repo/published/c2platform/core/)

## Example Playbook

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

```yaml
---
- name: Splunk
  hosts: splunk
  become: true

  roles:
    - { role: c2platform.core.linux }
    - { role: c2platform.mgmt.splunk }

  vars:
    deployment_task: check_splunk.yml
```

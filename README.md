# Ansible Collection - c2platform.mgmt

[![Pipeline Status](https://gitlab.com/c2platform/ansible-collection-mgmt/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/ansible-collection-mgmt/-/pipelines)
[![Latest Release](https://gitlab.com/c2platform/ansible-collection-mgmt/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/ansible-collection-mgmt/-/pipelines)
[![Ansible Galaxy](https://img.shields.io/badge/Galaxy-_c2platform.mgmt-blue.svg)](https://galaxy.ansible.com/ui/repo/published/c2platform/mgmt/)

C2 Platform management roles that are typically used within management domain.

## Roles

* [`backup`](./roles/backup) used by apps to configure backup and perform restore.
* [`awx`](./roles/awx) manage [Ansible Automation Platform (AAP) or
  AWX](https://c2platform.org/en/docs/concepts/ansible/aap).
* [`harbor`](./roles/harbor) provision and manage Harbor.
* [`splunk`](./roles/splunk) install and manage a Splunk cluster and Universal
  Forwarder nodes.

## Modules and Filters

For detailed information on the available modules and filters within this
collection, please refer to the
[Ansible Galaxy](https://galaxy.ansible.com/ui/repo/published/c2platform/mgmt/docs/)
page.

You can of course also use the `ansible-doc` command to explore the
documentation:

```bash
ansible-doc -t module --list c2platform.mgmt
ansible-doc -t filter --list c2platform.mgmt
ansible-doc -t filter <FILTER_NAME>
ansible-doc -t module <MODULE_NAME>
```

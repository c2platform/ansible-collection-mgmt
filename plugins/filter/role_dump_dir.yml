---
DOCUMENTATION:
  name: c2platform.mgmt.role_dump_dir
  author: onknows
  version_added: "1.0.0"
  short_description: >-
    Generates the directory path for role-specific database dumps.
  description: >-
    This filter constructs the directory path where database dumps for a specific
    role should be stored. It uses the role name and various variables to create
    a structured path.
  options:
    role:
      description: >-
        The name of the role for which the dump directory is being generated.
      required: True
      type: str
    vars:
      description: >-
        A dictionary of variables that includes necessary information such as
        backup temporary directory and inventory hostname.
      required: True
      type: dict
  examples:
    - name: Generate dump directory path for a role
      description: >-
        Generate the directory path where database dumps for the 'webapp' role
        should be stored.
      code: |
        "{{ 'webapp' | role_dump_dir(vars) }}"
  returns:
    path:
      description: >-
        The full directory path where the database dumps for the specified role
        should be stored.
      type: str
      returned: always
